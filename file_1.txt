Distributed version control
With distributed version control systems (DVCS), you don't rely on a central server to store all the versions of a projectís files. Instead, you clone a copy of a repository locally so that you have the full history of the
project. Two common distributed version control systems are Git and Mercurial.

While you don't have to have a central repository for your files, you may want one "central" place to keep 
your code so that you can share and collaborate on your project with others. That's where Bitbucket comes in. 
Keep a copy of your code in a repository on Bitbucket so that you and your teammates can use Git or Mercurial 
locally and to push and pull code